class Point {
    // Les attributs ! : abscisse et ordonnée
    private int x, y;
    
    //
    // Constructeur vide (défaut)
    //
    Point()
    {
	x = 0;
	y = 0;
    }

    //
    // Constructeur de copie
    //
    Point(Point p)
    {
	x = p.x;
	y = p.y;
    }
    
    
    //
    // Constructeur vraiment complet
    //
    Point(int a, int o)
    {
	x = a;
	y = o;
    }
    
    //
    // Translation d'un point
    //
    void translation(int dx, int dy)
    {
       x += dx;
       y += dy;
    }
}
